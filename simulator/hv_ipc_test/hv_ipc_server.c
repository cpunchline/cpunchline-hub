#include <pthread.h>
#include "ipc.h"

static void ipc_msg_handle(const ipc_msg_t *data)
{
    if (NULL == data)
    {
        return;
    }

    int ret = -1;

    if (E_IPC_MSG_ID_TEST_NOTIFY == data->msg_id)
    {
        if (data->send_len > 0)
        {
            PRINT_INFO("recv notify data[%s]", data->msg_data);
        }
        else
        {
            PRINT_INFO("recv notify data");
        }
    }
    else if (E_IPC_MSG_ID_TEST_ASYNC == data->msg_id)
    {
        if (data->send_len > 0)
        {
            PRINT_INFO("recv async data[%s]", data->msg_data);
        }
        else
        {
            PRINT_INFO("recv async data");
        }

        ret = ipc_send_resp(data, (const uint8_t *)"async resp", strlen("async resp"));
        if (0 != ret)
        {
            PRINT_ERROR("ipc_send_resp fail, ret[%d]", ret);
            return;
        }
    }
    else if (E_IPC_MSG_ID_TEST_SYNC == data->msg_id)
    {
        if (data->send_len > 0)
        {
            PRINT_INFO("recv sync data[%s]", data->msg_data);
        }
        else
        {
            PRINT_INFO("recv sync data");
        }

        ret = ipc_send_resp(data, (const uint8_t *)"sync resp", strlen("sync resp"));
        if (0 != ret)
        {
            PRINT_ERROR("ipc_send_resp fail, ret[%d]", ret);
            return;
        }
    }
    else
    {
        PRINT_ERROR("invalid msg_id[%u]! ", data->msg_id);
    }
}

void *ipc_thread_func(void *arg)
{
    (void)arg;
    ipc_run();

    return NULL;
}

int main()
{
    PRINT_INFO("hv_ipc_server start!");

    int ret = -1;
    ret = ipc_init(ipc_msg_handle);
    if (0 != ret)
    {
        PRINT_ERROR("ipc_init fail, ret[%d] ", ret);
        return -1;
    }

    pthread_t test_thread = -1;

    pthread_create(&test_thread, NULL, ipc_thread_func, NULL);
    pthread_detach(test_thread);

    while (1)
    {
        sleep(1);
    }
    ipc_destroy();

    return 0;
}
