#include <pthread.h>
#include "ipc.h"

htimer_t *once_timer = NULL;
htimer_t *period_timer = NULL;

static void ipc_msg_handle(const ipc_msg_t *data)
{
    if (NULL == data)
    {
        PRINT_ERROR("invalid params");
        return;
    }

    if (E_IPC_MSG_ID_TEST_NOTIFY == data->msg_id)
    {
        if (data->send_len > 0)
        {
            PRINT_INFO("recv notify data[%s]", data->msg_data);
        }
        else
        {
            PRINT_INFO("recv notify data");
        }
    }
    else if (E_IPC_MSG_ID_TEST_ASYNC == data->msg_id)
    {
        if (data->send_len > 0)
        {
            PRINT_INFO("recv async resp data[%s]", data->msg_data);
        }
        else
        {
            PRINT_INFO("recv async resp data");
        }
    }
    else
    {
        PRINT_ERROR("invalid msg_id[%u]!", data->msg_id);
    }
}

void test_once_hv_timeout_cb(htimer_t *t)
{
    (void)t;
    PRINT_DEBUG("ONCE TIMEOUT");
    int ret = ipc_send_async_req(E_IPC_ID_CLIENT, E_IPC_ID_SERVER, E_IPC_MSG_ID_TEST_ASYNC, (const uint8_t *)"async_req", strlen("async_req"), 1024, ipc_msg_handle, 1000);
    if (0 != ret)
    {
        PRINT_ERROR("ipc_send_async_req fail, ret[%d]", ret);
        return;
    }
}

void test_period_hv_interval_cb(htimer_t *t)
{
    (void)t;
    PRINT_DEBUG("PERIOD TIMEOUT");
    uint8_t sync_resp[1024] = {0};
    size_t sync_resp_len = sizeof(sync_resp);
    int ret = ipc_send_sync_req(E_IPC_ID_CLIENT, E_IPC_ID_SERVER, E_IPC_MSG_ID_TEST_SYNC, (const uint8_t *)"sync_req", strlen("sync_req"), sync_resp, &sync_resp_len, 1000);
    if (0 != ret)
    {
        PRINT_ERROR("ipc_send_sync_req fail, ret[%d]", ret);
        return;
    }

    PRINT_INFO("recv sync resp len[%zu], data[%s]", sync_resp_len, (char *)sync_resp);
}

void *ipc_thread_func(void *arg)
{
    (void)arg;
    ipc_run();

    return NULL;
}

int main()
{
    PRINT_INFO("hv_ipc_client start!");

    int ret = -1;
    ret = ipc_init(ipc_msg_handle);
    if (0 != ret)
    {
        PRINT_ERROR("ipc_init fail, ret[%d] ", ret);
        return -1;
    }

    pthread_t test_thread = -1;

    pthread_create(&test_thread, NULL, ipc_thread_func, NULL);
    pthread_detach(test_thread);

    // notify
    ret = ipc_send_notify(E_IPC_ID_CLIENT, E_IPC_ID_SERVER, E_IPC_MSG_ID_TEST_NOTIFY, NULL, 0);
    if (0 != ret)
    {
        PRINT_ERROR("ipc_send_notify fail, ret[%d]", ret);
        return -1;
    }

#if 1 // once timer
    once_timer = hv_timer_create(test_once_hv_timeout_cb, 1000, 1, NULL);
    if (NULL == once_timer)
    {
        PRINT_ERROR("once_timer is NULL");
        return -1;
    }
#endif

#if 1
    // period timer
    period_timer = hv_timer_create(test_period_hv_interval_cb, 5000, INFINITE, NULL);
    if (NULL == period_timer)
    {
        PRINT_ERROR("period_timer is NULL");
        return -1;
    }
#endif

    while (1)
    {
        sleep(1);
    }
    ipc_destroy();

    return 0;
}
