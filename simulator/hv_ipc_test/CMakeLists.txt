set(HV_IPC_CLIENT hv_ipc_client)
set(HV_IPC_SERVER hv_ipc_server)

set(HV_IPC_CLIENT_SRC hv_ipc_client.c)
set(HV_IPC_SERVER_SRC hv_ipc_server.c)

add_executable(${HV_IPC_CLIENT} ${HV_IPC_CLIENT_SRC})
target_link_libraries(${HV_IPC_CLIENT} PUBLIC pthread ipc_hv)
target_include_directories(${HV_IPC_CLIENT} PUBLIC ${CPUNCHLINE_LIB_DIR}/ipc_hv/inc)
target_include_directories(${HV_IPC_CLIENT} PUBLIC .)

add_executable(${HV_IPC_SERVER} ${HV_IPC_SERVER_SRC})
target_link_libraries(${HV_IPC_SERVER} PUBLIC pthread ipc_hv)
target_include_directories(${HV_IPC_SERVER} PUBLIC ${CPUNCHLINE_LIB_DIR}/ipc_hv/inc)
target_include_directories(${HV_IPC_SERVER} PUBLIC .)

install(TARGETS ${HV_IPC_CLIENT} ${HV_IPC_SERVER} DESTINATION bin)
