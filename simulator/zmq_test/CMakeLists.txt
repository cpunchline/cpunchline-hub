set(ZMQ_TEST_CLIENT zmq_test_client)
set(ZMQ_TEST_SERVER zmq_test_server)
set(ZMQ_TEST_PROXY zmq_test_proxy)

aux_source_directory(common/src ZMQ_TEST_COMMON_SRC_LIST)

# client
aux_source_directory(client ZMQ_TEST_CLIENT_SRC_LIST)
add_executable(${ZMQ_TEST_CLIENT} ${ZMQ_TEST_CLIENT_SRC_LIST} ${ZMQ_TEST_COMMON_SRC_LIST})
target_link_libraries(${ZMQ_TEST_CLIENT} PUBLIC zmq)
target_include_directories(${ZMQ_TEST_CLIENT} PUBLIC client)
target_include_directories(${ZMQ_TEST_CLIENT} PUBLIC common/inc)
target_include_directories(${ZMQ_TEST_CLIENT} PUBLIC ${CPUNCHLINE_PRE_BUILT_DIR}/include)
target_link_directories(${ZMQ_TEST_CLIENT} PUBLIC ${CPUNCHLINE_PRE_BUILT_DIR}/lib)

install(TARGETS ${ZMQ_TEST_CLIENT} DESTINATION bin)

# server
aux_source_directory(server ZMQ_TEST_SERVER_SRC_LIST)
add_executable(${ZMQ_TEST_SERVER} ${ZMQ_TEST_SERVER_SRC_LIST} ${ZMQ_TEST_COMMON_SRC_LIST})
target_link_libraries(${ZMQ_TEST_SERVER} PUBLIC zmq)
target_include_directories(${ZMQ_TEST_SERVER} PUBLIC server)
target_include_directories(${ZMQ_TEST_SERVER} PUBLIC common/inc)
target_include_directories(${ZMQ_TEST_SERVER} PUBLIC ${CPUNCHLINE_PRE_BUILT_DIR}/include)
target_link_directories(${ZMQ_TEST_SERVER} PUBLIC ${CPUNCHLINE_PRE_BUILT_DIR}/lib)

install(TARGETS ${ZMQ_TEST_SERVER} DESTINATION bin)

# proxy
aux_source_directory(proxy ZMQ_TEST_PROXY_SRC_LIST)
add_executable(${ZMQ_TEST_PROXY} ${ZMQ_TEST_PROXY_SRC_LIST} ${ZMQ_TEST_COMMON_SRC_LIST})
target_link_libraries(${ZMQ_TEST_PROXY} PUBLIC zmq)
target_include_directories(${ZMQ_TEST_PROXY} PUBLIC proxy)
target_include_directories(${ZMQ_TEST_PROXY} PUBLIC common/inc)
target_include_directories(${ZMQ_TEST_PROXY} PUBLIC ${CPUNCHLINE_PRE_BUILT_DIR}/include)
target_link_directories(${ZMQ_TEST_PROXY} PUBLIC ${CPUNCHLINE_PRE_BUILT_DIR}/lib)

install(TARGETS ${ZMQ_TEST_PROXY} DESTINATION bin)
