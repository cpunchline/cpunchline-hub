
#ifndef _SHM_H_
#define _SHM_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#define TEST_DATA_SHM_SIZE (1024)

#define SHM_RIGHT              (0666)
#define SHM_BLOCK_COUNT_MAX    (16)
#define SHM_MAGIC_NUM          (0x95279527)
#define SHM_BLOCK_SIZE_INVALID (0)

/* aligning multiples of 8 */
#define SHM_SIZE(size) (((size) + 7) / 8 * 8)

typedef struct _SHM_BLOCK
{
    unsigned int offset;
    unsigned int block_size; // aligned block size
    pthread_mutex_t mutex;
} SHM_BLOCK;

typedef struct _SHM_HEADER
{
    unsigned int magic_num;   // see SHM_MAGIC_NUM
    unsigned int refcount;    // total used shm modules count
    unsigned int block_count; // total shm block count
    unsigned int total_size;  // total aligned size
    pthread_mutex_t mutex;
    SHM_BLOCK block[SHM_BLOCK_COUNT_MAX];
    unsigned char shm_data[0];
} SHM_HEADER;

typedef struct _SHM_BLOCK_INFO
{
    unsigned int block_id; // enum SHM_BLOCK_ID
    unsigned int block_size;
    unsigned int align_block_size;
} SHM_BLOCK_INFO;

typedef enum _SHM_BLOCK_ID
{
    SHM_BLOCK_ID_TEST = 0,
} SHM_BLOCK_ID;

typedef struct _SHM_BLOCK_TEST
{
    char data[TEST_DATA_SHM_SIZE];
} SHM_BLOCK_TEST;

SHM_BLOCK_INFO *get_shm_blocks();
unsigned int get_shm_blocks_count();

unsigned int get_shm_align_header_size();
unsigned int get_shm_real_block_size(unsigned int block_id);
unsigned int get_shm_align_block_size(unsigned int block_id);
unsigned int get_shm_align_blocks_size();
unsigned int get_shm_align_total_size();

/**
 * @brief shm init
 * @return int : 0 is success , other failed
 */
int shm_init(void);

/**
 * @brief lock one block of shm.
 * @param[in] block_id : the block id of shm
 * @return int : 0 is success , other failed
 */
int shm_lock(unsigned int block_id);

/**
 * @brief unlock one block of shm.
 * @param[in] block_id : the block id of shm
 * @return int : 0 is success , other failed
 */
int shm_unlock(unsigned int block_id);

/**
 * @brief get addr of one block data.
 * You should call shm_lock() before call shm_get_block_addr(), and call shm_unlock() after.
 * @param[in] block_id : the block id of shm
 * @return void * : if fail, return NULL; if success, return the addr of the block data.
 */
void *shm_get_block_addr(unsigned int block_id);

/**
 * @brief copy one block data from shm to usr buffer.
 * @param[in] block_id : the block id of shm
 * @param[in] data_len : length of data_buf
 * @param[out] data_buf : usr buffer
 * @return int : 0 is success , other failed
 */
int shm_get_block_data(unsigned int block_id, unsigned int data_len, void *data_buf);

/**
 * @brief copy one block data from usr buffer to shm.
 * @param[in] block_id : the block id of shm
 * @param[in] data_len : length of data_buf
 * @param[out] data_buf : usr buffer
 * @return int : 0 is success , other failed
 */
int shm_set_block_data(unsigned int block_id, unsigned int data_len, void *data_buf);

#ifdef __cplusplus
}
#endif

#endif /* _SHM_H_ */
