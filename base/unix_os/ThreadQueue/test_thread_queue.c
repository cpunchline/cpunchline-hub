
#include <unistd.h>
#include "thread_queue.h"

thread_queue_t *p = NULL;

void *producer(void *arg)
{
    int32_t ret = THREAD_QUEUE_RET_SUCCESS;
    uint32_t data = 0;
    while (1)
    {
        data++;
        ret = thread_queue_push(p, &data, sizeof(data));
        if (THREAD_QUEUE_RET_SUCCESS != ret)
        {
            printf("thread_queue_push fail, ret[%d]\n", ret);
            exit(1);
        }
        printf("thread_queue_push success, data[%u]\n", data);
        usleep(100 * 1000);
    }
}

void *consumer(void *arg)
{
    int32_t ret = THREAD_QUEUE_RET_SUCCESS;
    uint32_t data = 0;
    uint32_t data_len = sizeof(data);
    while (1)
    {
        ret = thread_queue_pop_block(p, &data, &data_len, 1000);
        if (THREAD_QUEUE_RET_SUCCESS != ret)
        {
            printf("thread_queue_pop_block fail, ret[%d]\n", ret);
            continue;
        }

        printf("thread_queue_pop_block success, data[%u], data_len[%u]\n", data, data_len);
    }
}

int main()
{
    srand(time(NULL));
    p = thread_queue_create(5, sizeof(uint32_t));
    if (NULL == p)
    {
        return -1;
    }

    int ret = 0;
    pthread_t producer_tid;
    ret = pthread_create(&producer_tid, NULL, producer, NULL);
    if (0 != ret)
    {
        printf("pthread_create producer fail, ret[%d]\n", ret);
        return -1;
    }
    pthread_detach(producer_tid);

    pthread_t consumer_tid;
    ret = pthread_create(&consumer_tid, NULL, consumer, NULL);
    if (0 != ret)
    {
        printf("pthread_create consumer fail, ret[%d]\n", ret);
        return -1;
    }
    pthread_detach(consumer_tid);

    while (1)
    {
        sleep(1);
    }

    return 0;
}
