#include "thread_queue.h"

#define S_TO_MS  1000UL
#define S_TO_US  1000000ULL
#define S_TO_NS  1000000000ULL
#define MS_TO_US 1000UL
#define MS_TO_NS 1000000ULL

void thread_queue_delete(thread_queue_t **p)
{
    if (NULL == p || NULL == *p)
    {
        printf("p is NULL\n");
        return;
    }

    thread_queue_clear(*p);
    pthread_mutex_destroy(&(*p)->mutex);
    pthread_cond_destroy(&(*p)->cond);
    free(*p);
}

thread_queue_t *thread_queue_create(unsigned short node_num, unsigned int node_size)
{
    int32_t ret = THREAD_QUEUE_RET_SUCCESS;
    pthread_condattr_t condattr = {0};
    thread_queue_t *p = NULL;

    p = (thread_queue_t *)malloc(sizeof(thread_queue_t));
    if (NULL == p)
    {
        printf("p is NULL\n");
        return NULL;
    }
    memset(p, 0x00, sizeof(thread_queue_t));

    ret = pthread_mutex_init(&p->mutex, NULL);
    if (0 != ret)
    {
        free(p);
        return NULL;
    }

    pthread_condattr_init(&condattr);
    pthread_condattr_setclock(&condattr, CLOCK_MONOTONIC);
    ret = pthread_cond_init(&p->cond, &condattr);
    if (0 != ret)
    {
        pthread_mutex_destroy(&p->mutex);
        free(p);
        return NULL;
    }

    CIRCLEQ_INIT(&p->queue);
    for (unsigned short i = 0; i < node_num; ++i)
    {
        thread_queue_node_t *node = (thread_queue_node_t *)malloc(sizeof(thread_queue_node_t) + node_size);
        if (NULL == node)
        {
            printf("malloc fail\n");
            thread_queue_delete(&p);
            return NULL;
        }
        memset(node, 0x00, sizeof(thread_queue_node_t) + node_size);

        node->is_busy = false;
        node->is_extern = false;
        node->len = 0;
        node->p_exten = NULL;

        CIRCLEQ_INSERT_TAIL(&p->queue, node, entries);
    }
    p->used_count = 0;
    p->max_count = node_num;
    p->node_size = node_size;
    p->p_idle = CIRCLEQ_FIRST(&p->queue);
    p->p_used = CIRCLEQ_FIRST(&p->queue);

    return p;
}

int32_t thread_queue_push(thread_queue_t *p, void *data, uint32_t len)
{
    if (NULL == p || NULL == data || 0 == len)
    {
        return THREAD_QUEUE_RET_ERR_ARG;
    }

    pthread_mutex_lock(&p->mutex);
    if (p->used_count == p->max_count)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_FULL;
    }

    thread_queue_node_t *node = p->p_idle;
    if (node->is_busy)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_CONFLICT;
    }

    if (len > p->node_size)
    {
        node->p_exten = (uint8_t *)malloc(len);
        if (NULL == node->p_exten)
        {
            pthread_mutex_unlock(&p->mutex);
            return THREAD_QUEUE_RET_ERR_MEM;
        }
        memcpy(node->p_exten, data, len);
        node->is_extern = true;
    }
    else
    {
        memcpy(node->data, data, len);
    }

    node->len = len;
    node->is_busy = true;
    p->used_count++;

    // Update p_idle to the next idle node
    p->p_idle = CIRCLEQ_LOOP_NEXT(&p->queue, node, entries);
    pthread_cond_signal(&p->cond);
    pthread_mutex_unlock(&p->mutex);

    return THREAD_QUEUE_RET_SUCCESS;
}

static int32_t get_abs_timeout_time(struct timespec *ts_time, uint32_t timeout)
{
    struct timespec now_abs_time = {0};
    if (-1 == clock_gettime(CLOCK_MONOTONIC, &now_abs_time))
    {
        printf("clock_gettime fail, errno[%d](%s)\n", errno, strerror(errno));
        return -1;
    }

    uint64_t nsec = (now_abs_time.tv_sec * S_TO_NS) + (MS_TO_NS * timeout) + now_abs_time.tv_nsec;
    ts_time->tv_sec = (long)(nsec / S_TO_NS);
    ts_time->tv_nsec = (long)(nsec - (ts_time->tv_sec * S_TO_NS));

    return THREAD_QUEUE_RET_SUCCESS;
}

int32_t thread_queue_pop_block(thread_queue_t *p, void *data, uint32_t *len, uint32_t timeout)
{
    int32_t ret = THREAD_QUEUE_RET_SUCCESS;
    struct timespec ts_time = {0};
    thread_queue_node_t *node = NULL;

    if (NULL == p || NULL == data || NULL == len)
    {
        return THREAD_QUEUE_RET_ERR_ARG;
    }

    pthread_mutex_lock(&p->mutex);
    if (0 == p->used_count)
    {
        if (timeout == UINT32_MAX)
        {
            ret = pthread_cond_wait(&p->cond, &p->mutex);
            if (0 != ret)
            {
                pthread_mutex_unlock(&p->mutex);
                printf("pthread_cond_wait fail, ret[%d], errno[%d](%s)\n", ret, errno, strerror(errno));
                return THREAD_QUEUE_RET_ERR_OTHER;
            }
        }
        else
        {
            ret = get_abs_timeout_time(&ts_time, timeout);
            if (THREAD_QUEUE_RET_SUCCESS != ret)
            {
                printf("get_abs_timeout_time fail, ret[%d]\n", ret);
                pthread_mutex_unlock(&p->mutex);
                return ret;
            }

            ret = pthread_cond_timedwait(&p->cond, &p->mutex, &ts_time);
            if (0 != ret)
            {
                pthread_mutex_unlock(&p->mutex);
                if (ETIMEDOUT == ret)
                {
                    return THREAD_QUEUE_RET_ERR_TIMEOUT;
                }

                printf("pthread_cond_timedwait fail, ret[%d], errno[%d](%s)\n", ret, errno, strerror(errno));
                return THREAD_QUEUE_RET_ERR_OTHER;
            }
        }
    }

    node = p->p_used;
    if (!node->is_busy)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_NO_MSG;
    }

    if (node->len > *len)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_LEN;
    }

    if (node->is_extern && NULL != node->p_exten)
    {
        memcpy(data, node->p_exten, node->len);
        free(node->p_exten);
        node->p_exten = NULL;
    }
    else
    {
        memcpy(data, node->data, node->len);
    }

    node->is_busy = false;
    p->used_count--;
    *len = node->len;
    p->p_used = CIRCLEQ_LOOP_NEXT(&p->queue, node, entries);

    pthread_mutex_unlock(&p->mutex);

    return ret;
}

int32_t thread_queue_pop(thread_queue_t *p, void *data, uint32_t *len)
{
    if (NULL == p || NULL == data || NULL == len)
    {
        printf("p = %p data = %p\n", p, data);
        return THREAD_QUEUE_RET_ERR_ARG;
    }

    pthread_mutex_lock(&p->mutex);
    if (0 == p->used_count)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_NO_MSG;
    }

    thread_queue_node_t *node = p->p_used;
    if (!node->is_busy)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_NO_MSG;
    }

    if (node->len > *len)
    {
        pthread_mutex_unlock(&p->mutex);
        return THREAD_QUEUE_RET_ERR_LEN;
    }

    if (node->is_extern && NULL != node->p_exten)
    {
        memcpy(data, node->p_exten, node->len);
        free(node->p_exten);
        node->p_exten = NULL;
    }
    else
    {
        memcpy(data, node->data, node->len);
    }

    node->is_busy = false;
    p->used_count--;
    *len = node->len;
    p->p_used = CIRCLEQ_LOOP_NEXT(&p->queue, node, entries);

    pthread_mutex_unlock(&p->mutex);

    return THREAD_QUEUE_RET_SUCCESS;
}

void thread_queue_clear(thread_queue_t *p)
{
    if (NULL == p || CIRCLEQ_EMPTY(&p->queue))
    {
        printf("p is NULL or queue is empty\n");
        return;
    }

    pthread_mutex_lock(&p->mutex);
    thread_queue_node_t *node = NULL;
    CIRCLEQ_FOREACH(node, &p->queue, entries)
    {
        if (node->is_extern && NULL != node->p_exten)
        {
            free(node->p_exten);
        }
        node->is_extern = false;
        node->len = 0;
        node->is_busy = false;
    }

    p->used_count = 0;
    p->p_idle = CIRCLEQ_FIRST(&p->queue);
    p->p_used = CIRCLEQ_FIRST(&p->queue);
    pthread_mutex_unlock(&p->mutex);
}

bool thread_queue_is_full(thread_queue_t *p)
{
    pthread_mutex_lock(&p->mutex);
    bool ret = (p->used_count == p->max_count) ? true : false;
    pthread_mutex_unlock(&p->mutex);

    return ret;
}

bool thread_queue_is_empty(thread_queue_t *p)
{
    pthread_mutex_lock(&p->mutex);
    bool ret = (p->used_count == 0) ? true : false;
    pthread_mutex_unlock(&p->mutex);

    return ret;
}
