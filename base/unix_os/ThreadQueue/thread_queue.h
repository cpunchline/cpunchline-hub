#ifndef _THREAD_QUEUE_H_
#define _THREAD_QUEUE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/queue.h>

#define THREAD_QUEUE_RET_SUCCESS      0
#define THREAD_QUEUE_RET_ERR_ARG      1
#define THREAD_QUEUE_RET_ERR_FULL     2
#define THREAD_QUEUE_RET_ERR_MEM      3
#define THREAD_QUEUE_RET_ERR_LEN      4
#define THREAD_QUEUE_RET_ERR_CONFLICT 5
#define THREAD_QUEUE_RET_ERR_NO_MSG   6
#define THREAD_QUEUE_RET_ERR_EMPTY    7
#define THREAD_QUEUE_RET_ERR_TIMEOUT  8
#define THREAD_QUEUE_RET_ERR_OTHER    16

// clang-format off
typedef struct _thread_queue_node_t
{
    CIRCLEQ_ENTRY(_thread_queue_node_t) entries;
    bool is_busy;      // 标记该节点是否被占用。当节点中有数据时, 该值为1; 当节点空闲时, 该值为0。
    bool is_extern;    // 标记该节点中存储的数据是否超出了节点内部数据区的大小
    unsigned int len;  // 节点中数据的实际长度
    uint8_t *p_exten;  // 当数据大小超过了节点内部数据区的大小时, 这里将存储指向外部分配的内存的指针
    uint8_t data[];    // 存储节点中的实际数据
} thread_queue_node_t;

typedef struct _thread_queue_t
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    CIRCLEQ_HEAD(_thread_queue_head_t, _thread_queue_node_t) queue;
    unsigned short used_count;   // 当前队列中已使用的节点数量
    unsigned short max_count;    // 队列中节点的最大数量
    unsigned int node_size;      // 每个节点的数据区大小
    thread_queue_node_t *p_used; // 忙碌指针
    thread_queue_node_t *p_idle; // 空闲指针
} thread_queue_t;
// clang-foramt on

/**
 * @brief create a queue
 * @param[in] node_count: the number of queue nodes
 * @param[in] node_size: the size of a node
 * @return thread_queue_t * : success: not thread_queue_t pointer  failed: NULL
 */
thread_queue_t *thread_queue_create(unsigned short node_count, unsigned int node_size);

/**
 * @brief delete a queue
 * @param[in] p: thread_queue_t**
 * @return void :
 */
void thread_queue_delete(thread_queue_t **p);

/**
 * @brief push data to a queue
 * @param[in] p: A thread_queue_t pointer
 * @param[in] data : A data pointer to be push queue
 * @param[in] len : data buffer len
 * @return int32_t : reference the interface return value defination
 */
int32_t thread_queue_push(thread_queue_t *p, void *data, uint32_t len);

/**
 * @brief pop data from a queue in a blocking way
 * @param[in] p: A thread_queue_t pointer
 * @param[in] data : A data buffer that receives data from a queue
 * @param[in out] len : Data buffer len
 * @param[in] timeout : unit: ms, The wait time if the queue is empty
 * @return int32_t : reference the interface return value defination
 */
int32_t thread_queue_pop_block(thread_queue_t *p, void *data, uint32_t *len, uint32_t timeout);

/**
 * @brief pop data from a queue int a none-blocking way
 * @param[in] p: A thread_queue_t pointer
 * @param[in] data : A data buffer that receives data from a queue
 * @param[in out] len : Data buffer len
 * @return int32_t : reference the interface return value defination
 */
int32_t thread_queue_pop(thread_queue_t *p, void *data, uint32_t *len);

/**
 * @brief clear a queue data
 * @param[in] p: A thread_queue_t pointer
 * @return void :
 */
void thread_queue_clear(thread_queue_t *p);

/**
 * @brief judge a queue is full
 * @param[in] p: A thread_queue_t pointer
 * @return void :
 */
bool thread_queue_is_full(thread_queue_t *p);

/**
 * @brief judge a queue is empty
 * @param[in] p: A thread_queue_t pointer
 * @return void :
 */
bool thread_queue_is_empty(thread_queue_t *p);

#ifdef __cplusplus
}
#endif

#endif // _THREAD_QUEUE_H_
