#include <iostream>
#include <unistd.h>
#include "PosixThread.hpp"

void func(int &a)
{
    std::cout << &a << '\n';
}
void func2(const int &a)
{
    std::cout << &a << '\n';
}
struct X
{
    void f()
    {
        std::cout << "X::f\n";
    }
};

int main()
{
    std::cout << "main thread id: " << Posix_Thread::this_thread::get_id() << '\n';

    int a = 10;
    std::cout << &a << '\n';
    Posix_Thread::thread t{func, std::ref(a)};
    t.join();

    Posix_Thread::thread t2{func2, a};
    t2.join();

    Posix_Thread::thread t3{[]
                            {
                                std::cout << "thread id: " << Posix_Thread::this_thread::get_id() << '\n';
                            }};
    t3.join();

    X x;
    Posix_Thread::thread t4{&X::f, &x};
    t4.join();

    Posix_Thread::thread{[]
                         {
                             std::cout << "👉🤣\n";
                         }}
        .detach();
    sleep(1);
}
