set(POSIX_THREAD_APP posix_thread_app)

aux_source_directory(. POSIX_THREAD_APP_SRC_LIST)

add_executable(${POSIX_THREAD_APP} ${POSIX_THREAD_APP_SRC_LIST})
target_include_directories(${POSIX_THREAD_APP} PUBLIC .)

install(TARGETS ${POSIX_THREAD_APP} DESTINATION bin)
