#pragma once

#include "queue_spsc.h"

// 单生产者-多消费者(SPMC)循环缓冲区实现

namespace spmc
{

template <typename T>
class qring : public spsc::qring<T>
{
protected:
    using spsc::qring<T>::rd_;
    using spsc::qring<T>::wt_;
    using spsc::qring<T>::block_;
    using spsc::qring<T>::index_of;

public:
    /*
     * Yet another implementation of a lock-free circular array queue
     *  - Faustino Frechilla
     * https://www.codeproject.com/Articles/153898/Yet-another-implementation-of-a-lock-free-circular
     */
    std::tuple<T, bool> pop()
    {
        auto cur_rd = rd_.load(std::memory_order_relaxed);
        while (1)
        {
            auto id_rd = index_of(cur_rd);
            if (id_rd == index_of(wt_.load(std::memory_order_acquire)))
            {
                return {}; // empty
            }
            auto ret = std::make_tuple(block_[id_rd], true);
            if (rd_.compare_exchange_weak(cur_rd, cur_rd + 1, std::memory_order_release))
            {
                return ret;
            }
        }
    }
};

} // namespace spmc
