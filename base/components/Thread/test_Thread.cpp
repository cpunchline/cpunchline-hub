#include <iostream>
#include <vector>
#include "Thread.hpp"

void do_work(std::size_t id)
{
    std::cout << id << '\n';
}

void test()
{
    std::cout << std::this_thread::get_id() << '\n';
    EasyThread thread{[]
                  {
                      std::cout << std::this_thread::get_id() << '\n';
                  }};
    EasyThread thread2{std::move(thread)};
}

int main()
{
    // test();

    std::vector<std::thread> threads;
    for (std::size_t i = 0; i < 10; ++i)
    {
        threads.emplace_back(do_work, i); // 产生线程
    }
    for (auto &thread : threads)
    {
        thread.join(); // std::thread 需要对每个线程对象调用 join()
    }

    std::vector<EasyThread> threadss;
    for (std::size_t i = 0; i < 10; ++i)
    {
        threadss.emplace_back(do_work, i);
    }
    // EasyThread 无需对每个线程对象调用 join()
}
