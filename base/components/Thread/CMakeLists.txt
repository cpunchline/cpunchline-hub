set(MAKE_THREAD_APP make_thread_app)

aux_source_directory(. MAKE_THREAD_APP_SRC_LIST)

add_executable(${MAKE_THREAD_APP} ${MAKE_THREAD_APP_SRC_LIST})
target_include_directories(${MAKE_THREAD_APP} PUBLIC .)

install(TARGETS ${MAKE_THREAD_APP} DESTINATION bin)
