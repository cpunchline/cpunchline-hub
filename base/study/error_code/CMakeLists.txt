set(ERROR_CODE_APP error_code_app)

aux_source_directory(. ERROR_CODE_APP_SRC_LIST)

add_executable(${ERROR_CODE_APP} ${ERROR_CODE_APP_SRC_LIST})
target_include_directories(${ERROR_CODE_APP} PUBLIC .)

install(TARGETS ${ERROR_CODE_APP} DESTINATION bin)
