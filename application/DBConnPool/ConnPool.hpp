#pragma once

#include <queue>
#include <mutex>
#include <condition_variable> //条件变量

#include "MysqlConn.hpp"

using namespace std;

/*

数据库连接池
关于数据库连接池, 它主要是用在网络通信的服务器端, 在网络通信的服务器端有可能同时接收多个客户端请求, 并且客户端请求的数据它并不在服务器上, 而是在服务器对应的那个数据库服务器上。

一定要注意: 网络通信的服务器端, 它有可能部署了多个组件, 数据库服务器是这多个组件里边的其中一个。
又因为这个请求是多个客户端发送过来的, 所以服务器端为了提高读取数据的效率, 它需要通过多线程去访问数据库服务器。

在服务器端如果想要连接数据库服务器, 需要做四步操作: 
第一步: 通过tcp进行三次握手。因为网络通信的服务器端其实就是数据库的客户端。数据库的客户端连接数据库的服务器端, 其实是一个tcp通信, tcp通信的第一步就是需要进行三次握手。
第二步: 数据库服务器的连接认证。当连接建立之后, 数据库的服务器需要验证客户端的身份, 就是验证用户名和密码。验证成功之后, 数据库的客户端就可以通过sql语句去服务器端读取或者更新一些数据,
第三步: 数据库服务器关闭连接时的资源回收。当操作完成之后, 客户端和服务器需要断开连接, 在断开连接的时候需要进行资源的释放。
第四步: 断开通信连接的TCP四次挥手。数据库资源释放完了之后, 这个tcp通信还需要进行四次挥手。
把这四步操作做完了之后, 数据库的客户端和数据库的服务器端, 它们之间的通信也就结束了。如果说这个客户端和服务器端需要频繁地进行这四步操作, 很显然是非常的浪费时间的, 因此就提供了一个解决方案: 使用数据库连接池。

在这个数据库连接池里边, 我们需要事先和数据库的服务器建立若干个连接, 当需要将进行数据库操作的时候, 套接字通信的服务器端通过线程去连接池里取出一个可用的连接。
取出这个连接之后, 就可以和数据库的服务器直接通信了。通信结束之后, 这个连接是不需要进行断开的, 网络通信的服务器端对应的这个线程, 把这个数据库连接就还给数据库连接池。
因此我们就可以实现数据库连接的复用。通过这样的操作, 大大的降低了数据库连接的创建和销毁的次数, 对应的时间也就被节省出来了。
如果要实现一个数据库连接池, 它都需要有哪些组成部分?

这个数据库连接池, 它在程序中对应的肯定是一个对象。那么这个数据库连接池的对象有一个还是多个呢?
很显然, 一个就够了, 因为在数据库连接池里边, 它可以给我们提供若干个连接, 只需要一个数据库连接池对象就可以把多个连接取出来, 故这个对象一个就够。

因此, 在编写数据库连接池对应的类的时候, 这个类应该是一个单例模式的类。提供一个单例模式的类, 可以避免出创建多个数据库连接池对象。


如果空闲的连接太多了, 我们就需要销毁一部分, 那么我们怎么知道要销毁哪些数据库连接呢?
解决方案: 当我们创建出一个数据库之后, 它肯定是空闲的, 把这个时间点记录下来, 给它一个时间戳, 然后再提供一个线程专门的去检测这些数据库连接,
它们都空闲了多长的时间, 就是以现在的这个时间点减去对应的那个起始时间点,
如果这个时间长度超过了我们规定的时长, 那么就把对应的这个数据库连接给销毁掉。
如果没有超过规定的时长, 就让这个数据库连接继续存活。另外, 在实现这个数据库连接池的时候, 还涉及到一些线程相关的操作。
因为多个线程它们需要访问这个数据库连接池, 因此数据库连接池就是多个线程对应的共享资源。如果涉及到共享资源的访问, 就需要使用互斥锁。

网络通信的服务器端, 它提供的这多个线程在访问数据库连接池的时候, 把可用的数据库连接从连接池里边拿出去。
如果用完了, 之后再把这个可用的连接还给数据库连接池
。因此我们可以把这两部分(多线程和连接池->网络服务器)看成是一个生产者和消费者模型。
对应的消费者就是网络通信的服务器端提供的这多个线程。
生产者是我们在实现数据库连接的时候, 提供的一个额外的线程,
这个线程需要检测连接池里边的连接数量是否足够, 如果说这个连接已经不够了, 这个线程它就去生成新的连接, 如果说连接池里边的连接是足够的,
专门用于生产数据库连接的线程就不工作了。如果它不工作了, 就需要让线程阻塞。在C++11里边, 让线程池阻塞需要用到条件变量。

超时时长就是让线程阻塞等待的时间长度, 当线程等待了这个时间段之后, 再让它尝试去连接池里取出对应的连接。如果有就给它, 如果没有, 可以让它去等待, 或者直接告诉它没有了。

在连接池队列里边, 可用的有效连接太多了, 大部分都处于空闲的状态, 在这种情况下, 可以销毁一部分有效的数据库连接。销毁哪一个呢?
那就看哪一个数据库连接它的空闲时长到了我们指定的时间长度。如果到了就把它关闭, 相当于这个数据库连接就被我们认为释放了。故还需要在这个连接池里边给它添加两个属性,
一个是超时时长: m_timeout
一个是最大的空闲时长: m_maxIdleTime


单例模式有两种实现: 

(1) 懒汉模式: 在使用这个实例对象的时候, 才去创建它。单例模式的类肯定是有且只有一个的。懒汉模式的实现方式有很多种方式: 
1. 使用静态的局部变量; (在C++11里边使用静态局部变量是没有线程安全问题的)
2. 互斥锁(保证创建出来的实例对象有且仅有一个)
3. call_once函数(C++11提供的), 可以实现懒汉模式的单例类

(2) 饿汉模式: 不管我们用不用这个实例对象, 只要这个类被创建出来了, 那么对应的这个实例对象也就有了。饿汉模式是没有线程安全的问题的。
因为当我们把这个饿汉模式的实例对象创建出来此时如果有多个线程来访问这个单例对象, 不涉及到对象的创建。因此多线程在访问这个单例对象的时候, 肯定也是线程安全的。
1. 把构造函数设为私有的(private), 那如果我们想要得到这样的一个类的实例对象, 可以通过类名来获取到这个类的实例, 它能够访问的肯定是静态的函数或者静态变量。所以可以给这个类添加一个静态方法getConnPool();
2. 通过拷贝构造函数也可以创建出对应的实例对象, 所以应该把拷贝构造和拷贝赋值函数也设置成私有的(private)。或者说把这个拷贝构造函数直接删除掉=delete
ConnPool* ConnPool::getConnPool()
{
    static ConnPool pool;
    return &pool;
}

class ConnPool
{
public:
    static ConnPool* getConnPool();                    // 获得单例对象
    ConnPool(const ConnPool& obj) = delete;            // 删除拷贝构造函数
    ConnPool& operator=(const ConnPool& obj) = delete; // 删除拷贝赋值运算符重载函数
private:
    ConnPool(); // 构造函数私有化
};


https://github.com/open-source-parsers/jsoncpp
*/

class ConnPool
{
public:
    static ConnPool *getConnPool();                    // 获得单例对象
    ConnPool(const ConnPool &obj) = delete;            // 删除拷贝构造函数
    ConnPool &operator=(const ConnPool &obj) = delete; // 删除拷贝赋值运算符重载函数
    shared_ptr<MysqlConn> getConn();                   // 从连接池中取出一个连接
    ~ConnPool();                                       // 析构函数
private:
    ConnPool();           // 构造函数私有化
    bool parseJsonFile(); // 解析json格式文件
    void produceConn();   // 生产数据库连接
    void recycleConn();   // 销毁数据库连接
    void addConn();       // 添加数据库连接

    // 连接服务器所需信息
    string m_ip;           // 数据库服务器ip地址
    string m_user;         // 数据库服务器用户名
    string m_dbName;       // 数据库服务器的数据库名
    string m_passwd;       // 数据库服务器密码
    unsigned short m_port; // 数据库服务器绑定的端口

    // 连接池信息 选择队列, 存储数据库连接对象
    std::queue<MysqlConn *> m_connQ;
    unsigned int m_maxSize;    // 连接数上限值
    unsigned int m_minSize;    // 连接数下限值
    int m_timeout;             // 连接超时时长
    int m_maxIdleTime;         // 最大的空闲时长
    mutex m_mutexQ;            // 独占互斥锁
    condition_variable m_cond; // 条件变量
};
