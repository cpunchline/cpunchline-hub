#include <stdio.h>
#include <stdlib.h>
#include "debug_backtrace.h"

static void foo(void)
{
    char *tmp = NULL;
    *tmp = 0;
    printf("xxx=%s\n", tmp);
    free(tmp);
    free(tmp);
}

static void foo2(void)
{
    foo();
}

static void foo3(void)
{
    foo2();
}

int main()
{
    debug_backtrace_init();
    foo3();

    return 0;
}
