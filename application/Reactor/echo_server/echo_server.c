#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/types.h>

#define IPADDRESS   "127.0.0.1"
#define PORT        8787
#define MAXSIZE     1024
#define LISTENQ     5
#define FDSIZE      1000
#define EPOLLEVENTS 100

// 长链接, 一个简易的基于epoll事件循环的TCP服务端程序

void add_event(int epollfd, int fd, int state)
{
    struct epoll_event event = {};
    event.events = state;
    event.data.fd = fd;
    epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &event);
}

void modify_event(int epollfd, int fd, int state)
{
    struct epoll_event event = {};
    event.events = state;
    event.data.fd = fd;
    epoll_ctl(epollfd, EPOLL_CTL_MOD, fd, &event);
}

void delete_event(int epollfd, int fd, int state)
{
    struct epoll_event event = {};
    event.events = state;
    event.data.fd = fd;
    epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, &event);
}

void handle_accept(int epollfd, int listenfd)
{
    int connfd = -1;
    struct sockaddr_in connaddr = {};
    socklen_t connaddrlen = sizeof(connaddr);

    connfd = accept(listenfd, (struct sockaddr *)&connaddr, &connaddrlen);
    if (connfd < 0)
    {
        perror("accept error!");
    }
    else
    {
        printf("accept new client: %s:%d\n", inet_ntoa(connaddr.sin_addr), connaddr.sin_port);
        add_event(epollfd, connfd, EPOLLIN | EPOLLERR | EPOLLHUP | EPOLLRDHUP);
    }
}

void do_read(int epollfd, int fd, char *buf)
{
    ssize_t nread = read(fd, buf, MAXSIZE);
    if (nread <= 0)
    {
        if (0 == nread)
        {
            fprintf(stderr, "client close!\n");
        }
        else
        {
            perror("read error!");
        }
        close(fd);
        delete_event(epollfd, fd, EPOLLIN);
    }
    else
    {
        printf("read msg[%s]\n", buf);
        modify_event(epollfd, fd, EPOLLOUT | EPOLLERR | EPOLLHUP | EPOLLRDHUP);
    }
}

void do_write(int epollfd, int fd, char *buf)
{
    printf("write msg[%s]\n", buf);
    ssize_t nwrite = write(fd, buf, strlen(buf));
    if (nwrite < 0)
    {
        perror("write error!");
        close(fd);
        delete_event(epollfd, fd, EPOLLOUT | EPOLLERR | EPOLLHUP | EPOLLRDHUP);
    }
    else
    {
        modify_event(epollfd, fd, EPOLLIN | EPOLLERR | EPOLLHUP | EPOLLRDHUP);
    }
    memset(buf, 0, MAXSIZE);
}

void handle_events(int epollfd, struct epoll_event *events, int num, int listenfd, char *buf)
{
    for (int i = 0; i < num; ++i)
    {
        int fd = events[i].data.fd;

        if (events[i].events & (EPOLLERR | EPOLLHUP))
        {
            // 对端 给一个已经关闭的socket read/write
            printf("local read/write a closed socket!\n");
            close(fd);
        }
        else if (events[i].events & (EPOLLRDHUP))
        {
            // 检测对端关闭写入端的情况
            // 本端检测到对端关闭(对端发送了FIN包)
            printf("peer close!\n");
            close(fd);
        }
        else if (events[i].events & EPOLLIN)
        {
            if (fd == listenfd)
            {
                handle_accept(epollfd, listenfd);
            }
            else
            {
                do_read(epollfd, fd, buf);
            }
        }
        else if (events[i].events & EPOLLOUT)
        {
            do_write(epollfd, fd, buf);
        }
    }
}

void do_epoll(int listenfd)
{
    int epollfd = -1;
    int ret = -1;
    struct epoll_event events[EPOLLEVENTS] = {};
    char buf[MAXSIZE] = {};

    epollfd = epoll_create1(EPOLL_CLOEXEC);
    add_event(epollfd, listenfd, EPOLLIN | EPOLLERR | EPOLLHUP | EPOLLRDHUP);

    for (;;)
    {
        ret = epoll_wait(epollfd, events, EPOLLEVENTS, -1);
        handle_events(epollfd, events, ret, listenfd, buf);
    }

    close(epollfd);
}

int main()
{
    int listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listenfd < 0)
    {
        perror("socket error!");
        exit(1);
    }

    // 设置端口重用选项
    int reuse = 1;
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0)
    {
        perror("setsockopt error!");
        close(listenfd);
        exit(1);
    }

    struct sockaddr_in servaddr = {};
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    inet_pton(AF_INET, IPADDRESS, &servaddr.sin_addr);
    servaddr.sin_port = htons(PORT);

    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)))
    {
        perror("bind error!");
        exit(1);
    }
    listen(listenfd, LISTENQ);
    do_epoll(listenfd);

    return 0;
}
