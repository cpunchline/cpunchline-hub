set(REACTOR_DIR ${CPUNCHLINE_APP_DIR}/Reactor)
set(REACTOR_CPP_SERVER_DIR ${REACTOR_DIR}/cpp_server)

add_subdirectory(echo_server)
add_subdirectory(${REACTOR_CPP_SERVER_DIR})
