#include <arpa/inet.h>
#include <bits/stdc++.h>
#include <liburing.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// sqe 提交队列
// cqe 完成队列

const int BUFSIZE = 1024;
struct request
{
    enum STATE
    {
        ACCEPT,
        READ,
        WRITE
    };
    int fd;
    STATE state;
    union
    {
        struct
        {
            sockaddr_in ipv4_addr;
            socklen_t lens;
        } addr;
        char buf[BUFSIZE];
    };
};

class IOuring
{
    io_uring ring;

public:
    IOuring(int queue_size)
    {
        io_uring_queue_init(queue_size, &ring, 0); // 从环中得到一块空位
    }

    ~IOuring()
    {
        io_uring_queue_exit(&ring); // 退出
    }

    void seen(io_uring_cqe *cqe)
    {
        io_uring_cqe_seen(&ring, cqe); // 将任务移出完成队列
    }

    int wait(io_uring_cqe **cqe)
    {
        return io_uring_wait_cqe(&ring, cqe); // 阻塞等待一项完成的任务
    }

    int submit()
    {
        return io_uring_submit(&ring); // 提交任务到提交队列
    }

    void accept_asyn(int sock_fd, request *body)
    {
        auto sqe = io_uring_get_sqe(&ring);
        body->state = request::ACCEPT;
        body->fd = sock_fd;
        body->addr.lens = sizeof(sockaddr_in);
        io_uring_prep_accept(sqe, sock_fd, (sockaddr *)&(body->addr.ipv4_addr),
                             &(body->addr.lens), 0);
        io_uring_sqe_set_data(sqe, body);
    }

    void read_asyn(int client_fd, request *body)
    {
        auto sqe = io_uring_get_sqe(&ring);
        body->state = request::READ;
        body->fd = client_fd;
        io_uring_prep_read(sqe, client_fd, body->buf, sizeof(body->buf), -1);
        io_uring_sqe_set_data(sqe, body);
    }

    void write_asyn(int client_fd, request *body)
    {
        auto sqe = io_uring_get_sqe(&ring);
        body->state = request::WRITE;
        body->fd = client_fd;
        io_uring_prep_write(sqe, client_fd, body->buf, sizeof(body->buf), -1);
        io_uring_sqe_set_data(sqe, body);
    }
};

int main()
{
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr_in sock_addr;
    sock_addr.sin_port = htons(8000);
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_addr.s_addr = INADDR_ANY;
    int ret = bind(sock_fd, (sockaddr *)&sock_addr, sizeof(sock_addr));
    if (0 != ret)
    {
        std::cout << "bind error " << std::endl;
        return -1;
    }

    listen(sock_fd, 10);

    IOuring ring(BUFSIZE);

    ring.accept_asyn(sock_fd, new request);
    ring.submit();

    while (true)
    {
        io_uring_cqe *cqe;
        ring.wait(&cqe);
        request *res = (request *)cqe->user_data;
        switch (res->state)
        {
            case request::ACCEPT:
                if (cqe->res > 0)
                {
                    int client_fd = cqe->res;
                    ring.accept_asyn(sock_fd, res);
                    ring.read_asyn(client_fd, new request);
                    ring.submit();
                }
                std::cout << cqe->res << std::endl;
                break;
            case request::READ:
                if (cqe->res > 0)
                    std::cout << res->buf << std::endl;
                ring.write_asyn(res->fd, res);
                ring.submit();
                break;
            case request::WRITE:
                if (cqe->res > 0)
                {
                    close(res->fd);
                    delete res;
                }
                break;
            default:
                std::cout << "error " << std::endl;
                break;
        }
        ring.seen(cqe);
    }

    return 0;
}
