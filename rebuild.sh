#!/bin/bash

# White
loginfo()
{
    echo -e "\033[37m$1\033[0m"
}

# Yellow
logwarn()
{
    echo -e "\033[33m$1\033[0m"
}

# Green
logok()
{
    echo -e "\033[32m$1\033[0m"
}

# Red
logerr()
{
    echo -e "\033[31m$1\033[0m"
}

ROOT_DIR=$(cd `dirname $0`; pwd)
OUTPUT_DIR=$ROOT_DIR/output
BUILD_DIR=$ROOT_DIR/build

rm -rf $BUILD_DIR
# rm -rf $BUILD_DIR/CMakeCache.txt
rm -rf $OUTPUT_DIR

# export PKG_CONFIG_PATH="$ROOT_DIR/tools/pre_built/lib/pkgconfig:$PKG_CONFIG_PATH"
cmake -S $ROOT_DIR -B $BUILD_DIR -G "Unix Makefiles"
cmake --build $BUILD_DIR --target install --config Release --parallel $(nproc)

# 获取各target的依赖关系
# cmake --graphviz=build.dot $ROOT_DIR
# sudo apt install graphviz
# dot -Tpng -o build.png build.dot
# dot -Tpdf -o build.pdf build.dot

# 静态检查
# cmake --build $BUILD_DIR --target clang-tidy
