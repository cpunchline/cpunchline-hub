#ifndef _IPC_ZMQ_H_
#define _IPC_ZMQ_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define IPC_ZMQ_S_TO_MS  (1000)
#define IPC_ZMQ_MS_TO_US (1000)

#define D_IPC_ZMQ_MODULE_NAME            (64)
#define D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX (256)

#define D_IPC_ZMQ_BROADCAST_TOPIC     1
#define D_IPC_ZMQ_BROADCAST_TOPIC_STR "1"

// connect timeout
#define D_IPC_ZMQ_NOTIFY_CONNECT_TIMEOUT    (1000)
#define D_IPC_ZMQ_SYNC_CONNECT_TIMEOUT      (1000)
#define D_IPC_ZMQ_ASYNC_CONNECT_TIMEOUT     (1000)
#define D_IPC_ZMQ_BROADCAST_CONNECT_TIMEOUT (1000)

// wait timeout
#define D_IPC_ZMQ_WAIT_CONTINUE          (-1)
#define D_IPC_ZMQ_NOTIFY_WAIT_TIMEOUT    (1500)
#define D_IPC_ZMQ_ASYNC_WAIT_TIMEOUT     (20)
#define D_IPC_ZMQ_BROADCAST_WAIT_TIMEOUT (1000)
// sync wait timeout by self set

#define D_INPROC_ZMQ_ASYNC_DEALER_ADDRESS    "inproc://tbox-%04x-async-dealer-response"
#define D_INPROC_ZMQ_BACK_REQ_RESP_ADDRESS   "inproc://tbox-%04x-back-response"
#define D_IPC_ZMQ_REQ_RESP_ADDRESS           "ipc:///tmp/tbox-%04x-req-resp.ipc"
#define D_IPC_ZMQ_BROADCAST_FRONTEND_ADDRESS "ipc:///tmp/tbox-broadcast-frontend.ipc"
#define D_IPC_ZMQ_BROADCAST_BACKEND_ADDRESS  "ipc:///tmp/tbox-broadcast-backend.ipc"

typedef enum _ipc_zmq_module_id_e
{
    E_IPC_ZMQ_MODULE_ID_INVALID = 0,

    E_IPC_ZMQ_MODULE_ID_MANAGER = 1,
    E_IPC_ZMQ_MODULE_ID_TEST1 = 2,
    E_IPC_ZMQ_MODULE_ID_TEST2 = 3,

    E_IPC_ZMQ_MODULE_ID_MAX = 50
} ipc_zmq_module_id_e;

typedef enum _ipc_zmq_workmode_e
{
    E_IPC_ZMQ_WORKMODE_SINGLE_WORKER = 1,
    E_IPC_ZMQ_WORKMODE_MULT_WORKER = 2,
} ipc_zmq_workmode_e;

typedef enum _ipc_zmq_msg_type_e
{
    E_IPC_ZMQ_MSG_TYPE_INVALID = 0,
    E_IPC_ZMQ_MSG_TYPE_NOTIFY,    // notify
    E_IPC_ZMQ_MSG_TYPE_SYNC,      // sync
    E_IPC_ZMQ_MSG_TYPE_ASYNC,     // async
    E_IPC_ZMQ_MSG_TYPE_BROADCAST, // broadcast
    E_IPC_ZMQ_MSG_TYPE_HEARTBEAT, // heartbeat(TODO)
} ipc_zmq_msg_type_e;

typedef enum _ipc_zmq_msg_id_e
{
    E_IPC_ZMQ_MSG_ID_INVALID = 0,
} ipc_zmq_msg_id_e;

typedef struct _ipc_zmq_broadcast_msg_baseinfo_t
{
    uint8_t sub; // "1" mean broadcast
    uint32_t src_id;
    uint32_t msg_type;
    uint32_t msg_id;
} ipc_zmq_broadcast_msg_baseinfo_t;

typedef struct _ipc_zmq_msg_baseinfo_t
{
    uint32_t src_id;
    uint32_t dest_id;
    uint32_t msg_type;
    uint32_t msg_id;
    size_t send_len;
    size_t recv_len;
} ipc_zmq_msg_baseinfo_t;

// clang-format off
typedef void (*PF_IPC_ZMQ_NOTIFY_HANDLER)(uint32_t src_id, uint32_t msg_id, const void *notify_data, size_t notify_data_len);
typedef void (*PF_IPC_ZMQ_ASYNC_HANDLER)(uint32_t src_id, uint32_t msg_id, void *response_data);
typedef void (*PF_IPC_ZMQ_BROADCAST_HANDLER)(uint32_t src_id, uint32_t msg_id, const void *broadcast_data);
typedef void (*PF_IPC_ZMQ_RESPONSE_HANDLER)(uint32_t src_id, uint32_t msg_id, const void *indata, void *outdata);
// clang-format on

typedef struct _ipc_zmq_register_info_t
{
    uint32_t module_id; // see ipc_zmq_module_id_e
    char module_name[D_IPC_ZMQ_MODULE_NAME];
    uint32_t work_mode;
    PF_IPC_ZMQ_NOTIFY_HANDLER notify_handler;
    PF_IPC_ZMQ_RESPONSE_HANDLER response_handler; // async/sync/notify, it is a RPC impl
    PF_IPC_ZMQ_ASYNC_HANDLER async_handler;
    PF_IPC_ZMQ_BROADCAST_HANDLER broadcast_handler;
} ipc_zmq_register_info_t;

typedef struct _ipc_zmq_manager_info_t
{
    ipc_zmq_register_info_t reg_info;
    void *ctx;
    void *pub;
    char mult_reqbackaddress[D_IPC_ZMQ_SOCKET_ADDRESS_LEN_MAX];
    void *rep;
    void *rep_thread;
    void *async_thread;
    bool is_run;
} ipc_zmq_manager_info_t;

int32_t ipc_zmq_init(ipc_zmq_register_info_t *reg_info);
int32_t ipc_zmq_destroy(uint32_t module_id);
int32_t ipc_zmq_send_notify(uint32_t src_id, uint32_t dest_id, uint32_t msg_id, const void *notify_data, size_t notify_data_len);
int32_t ipc_zmq_send_sync(uint32_t src_id, uint32_t dest_id, uint32_t msg_id, const void *sync_req_data, size_t sync_req_data_len, void *sync_resp_data, size_t *sync_resp_data_len, size_t sync_resp_data_max_len, long timeout);
int32_t ipc_zmq_send_async(uint32_t src_id, uint32_t dest_id, uint32_t msg_id, const void *async_req_data, size_t async_req_data_len, size_t async_resp_data_len);
int32_t ipc_zmq_send_broadcast(uint32_t src_id, uint32_t msg_id, const void *broadcast_data, size_t broadcast_data_len);

#endif // _IPC_ZMQ_H_
