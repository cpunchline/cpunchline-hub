#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <fcntl.h>
#include <time.h>
#include <sys/prctl.h>

#include "hv/hv.h"
#include "hv/hloop.h"

#ifdef __cplusplus
extern "C"
{
#endif

// 日志开关
#define __DEBUG__ 1
#if __DEBUG__

#ifdef WIN32 // Windows
#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '\\') + (1))
#endif
#elif __GNUC__ // Linux
#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') + (1))
#endif
#endif

#define MAX_DATE_TIME_LEN 64

#define DEBUG_PRINTF(level, format, ...)                                                                                            \
    do                                                                                                                              \
    {                                                                                                                               \
        time_t now = time(NULL);                                                                                                    \
        struct tm timeinfo = {};                                                                                                    \
        localtime_r(&now, &timeinfo);                                                                                               \
        char dateTime[MAX_DATE_TIME_LEN] = {};                                                                                      \
        strftime(dateTime, sizeof(dateTime), "%Y-%m-%d %H:%M:%S", &timeinfo);                                                       \
        fprintf(stdout, "[%s] [%s] [%s:%d %s] " format "\n", dateTime, level, __FILENAME__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
    } while (0)

#define PRINT_CRIT(...)  DEBUG_PRINTF("LOG_CRIT", ##__VA_ARGS__);
#define PRINT_ERROR(...) DEBUG_PRINTF("LOG_ERROR", ##__VA_ARGS__);
#define PRINT_WARN(...)  DEBUG_PRINTF("LOG_WARN", ##__VA_ARGS__);
#define PRINT_INFO(...)  DEBUG_PRINTF("LOG_INFO", ##__VA_ARGS__);
#define PRINT_DEBUG(...) DEBUG_PRINTF("LOG_DEBUG", ##__VA_ARGS__);

#define UNIX_SOCKET_PATH_PREFIX       "/tmp/ipc_socket"
#define UNIX_SOCKET_NAME_LEN_MAX      (256)
#define UNIX_SOCKET_PATH_PREFIX_RIGHT 0755
#define UNIX_SOCKET_RIGHT             0766

#endif

#define IPC_CONNECT_TIMEOUT (1000)
#define IPC_WRITE_TIMEOUT   (1000)
#define IPC_READ_TIMEOUT    (1000)
#define IPC_CLOSE_TIMEOUT   (1000)

typedef enum _ipc_id_e
{
    E_IPC_ID_INVALID,
    E_IPC_ID_SERVER = 1, // hv_ipc_server
    E_IPC_ID_CLIENT = 2, // hv_ipc_client

    E_IPC_ID_MAX = UINT32_MAX
} ipc_id_e;

typedef enum _ipc_msg_id_e
{
    E_IPC_MSG_ID_INVALID = 0,
    E_IPC_MSG_ID_TEST_NOTIFY,
    E_IPC_MSG_ID_TEST_ASYNC,
    E_IPC_MSG_ID_TEST_SYNC,

} ipc_msg_id_e;

typedef enum _ipc_msg_type_e
{
    E_IPC_MSG_TYPE_INVALUD = 0,
    E_IPC_MSG_TYPE_NOTIFY = 1,
    E_IPC_MSG_TYPE_ASYNC = 2,
    E_IPC_MSG_TYPE_SYNC = 3,
    E_IPC_MSG_TYPE_RESP = 4,
} ipc_msg_type_e;

typedef void *ipc_handle_t;

struct _ipc_msg_t;
typedef void (*ipc_handle_cb_t)(const struct _ipc_msg_t *msg);
typedef struct _ipc_msg_t
{
    ipc_handle_t hd;
    uint32_t src;
    uint32_t dest;
    uint32_t msg_type; // 1 notify, 2 async, 3 sync req, 4 sync/async resp
    uint32_t msg_id;
    ipc_handle_cb_t async_cb;
    size_t timeout;
    size_t send_len;
    size_t recv_max_len; // only used in sync/async resp
    uint8_t msg_data[];
} ipc_msg_t;

int ipc_init(ipc_handle_cb_t ipc_cb); // one-acceptor-multi-workers
int ipc_run();
void ipc_destroy();

int ipc_send_notify(uint32_t src, uint32_t dest, uint32_t msg_id, const uint8_t *notify_data, size_t notify_len);
int ipc_send_async_req(uint32_t src, uint32_t dest, uint32_t msg_id, const uint8_t *req_data, size_t req_len, size_t resp_max_len, ipc_handle_cb_t ipc_async_cb, uint32_t timeout);
int ipc_send_sync_req(uint32_t src, uint32_t dest, uint32_t msg_id, const uint8_t *req_data, size_t req_len, uint8_t *resp_data, size_t *resp_len, uint32_t timeout);
int ipc_send_resp(const ipc_msg_t *req, const uint8_t *resp_data, size_t resp_len);

// timer
htimer_t *hv_timer_create(htimer_cb cb, uint32_t timeout, uint32_t repeat, void *timeoutdata);
void hv_timer_destroy(htimer_t *timer);

#ifdef __cplusplus
}
#endif
