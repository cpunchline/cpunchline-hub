set(CPUNCHLINE_VERSION "1.0.1")

find_package(Git QUIET)

if(GIT_FOUND)
    execute_process(
        COMMAND ${GIT_EXECUTABLE} log -n 1 --pretty=format:%h
        OUTPUT_VARIABLE COMMTI_ID
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_QUIET
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    )
    execute_process(
        COMMAND ${GIT_EXECUTABLE} log -n 1 --pretty=format:%ci
        OUTPUT_VARIABLE COMMIT_TIME
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_QUIET
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    )
endif()

string(TIMESTAMP BUILD_TIME "%Y-%m-%d %H:%M:%S")

file(WRITE "${CPUNCHLINE_OUTPUT_DIR}/cpunchline_version"
    "CPUNCHLINE_VERSION = ${CPUNCHLINE_VERSION}\n"
    "COMMTI_ID          = ${COMMTI_ID}\n"
    "COMMIT_TIME        = ${COMMIT_TIME}\n"
    "BUILD_TIME         = ${BUILD_TIME}\n"
)
